@extends('layouts.default')
@section('content') 
<div class="container">
  <div class="descrip">
    <h2 class="de">Theater</h2>
  </div>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="{{asset('img/lamour-1.png')}}" style="width:100%;">
        <div class="carousel-caption">
          <h2>Giường nằm êm ái</h2>
          <p>Ghế ngồi là giường cùng chăn mang lại cảm giác ấm áp và tinh tế</p>
        </div>
      </div>

      <div class="item">
        <img src="{{asset('img/lamour-3.png')}}" style="width:100%;">
        <div class="carousel-caption">
          <h2>Giường nằm êm ái</h2>
          <p>Ghế ngồi là giường cùng chăn mang lại cảm giác ấm áp và tinh tế</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="tabs">
<ul>
  <li class="first" style="border-right: 1px solid white"><a href="/lichchieu">Lịch chiếu</a></li>
  <li class="second"><a href=""><strong>Giá vé</strong></a></li>
</ul>
</div>
<div class="container information-ticket-new">
<div class="box-table-price">
<div class="title-table-price">
<h2>BẢNG GIÁ VÉ</h2>
</div>
<table border="1">
<tbody>
<tr class="title-day">
<td>
<p>Từ thứ hai đến thứ sáu</p>
<td colspan="3">
<p>thứ hai, thứ ba, thứ năm</p>
</td>
<td rowspan="2">
<p>thứ tư vui vẻ</p>
</td>
<td colspan="3">
<p>thứ sáu, thứ bảy, chủ nhật và ngày lễ</p>
</td>
</tr>
<tr class="title-time">
<td class="w12-5">
<p>Thành Viên Từ 22 Tuổi Trở Xuống, Sở Hữu Thẻ U22, Tại Rạp</p>
</td>
<td class="w12-5">
<p>trẻ em</p>
</td>
<td class="w12-5">
<p>học sinh, sinh viên, người lớn tuổi</p>
</td>
<td class="w12-5">
<p>người lớn</p>
</td>
<td class="w12-5">
<p>trẻ em</p>
</td>
<td class="w12-5">
<p>học sinh, sinh viên (trước 17:00), Người Cao Tuổi</p>
</td>
<td class="w12-5">
<p>người lớn</p>
</td>
</tr>
<tr class="value">
<td>65.000</td>
<td>60.000</td>
<td>70.000</td>
<td>85.000</td>
<td>75.000</td>
<td>70.000</td>
<td>80.000</td>
<td>105.000</td>
</tr>
<tr class="sur">
<td colspan="8">
<p class="title-surcharge"><strong>PHỤ THU:</strong></p>
<ul class="price-sub-table">
<li class="sub-charge">
<p>Ghế VIP:</p>
+5.000 ( Miễn phụ thu cho U22 )</li>
<li class="sub-charge">
<p>Deluxe:</p>
+10.000</li>
<li class="sub-charge">
<p>Sweetbox :</p>
<ul>
<li>+25.000 (Thứ Hai, Thứ Ba, Thứ Năm, Thứ Sáu, Thứ Bảy, Chủ Nhật )</li>
<li>+15.000 (Thứ Tư )</li>
</ul>
</li>
<li class="sub-charge">
<p>3D :</p>
<ul>
<li>+30.000 (Thứ Hai, Thứ Ba, Thứ Năm)</li>
<li>+50.000 (Thứ Sáu, Thứ Bảy, Chủ Nhật và Ngày Lễ )</li>
</ul>
</li>
<li class="sub-charge">
<p>Lễ/Tết :</p>
<ul>
<li>+10.000</li>
</ul>
</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
</div>
@endsection
<style type="text/css">
  .title-table-price {
    width: 100%;
    float: left;
    padding: 5px 0px;
    background: #333;
    text-align: center;
}
.title-table-price h2 {
    display: inline !important;
    font-family: Verdana,Arial,sans-serif;
    font-size: 20px;
    margin: 0;
    width: 100%;
    text-transform: uppercase;
    color: #fff;
}
.information-ticket-new table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    text-align: center;
    font-size: 11px;
    font-family: Verdana,Arial,sans-serif;
    color: #636363;
    line-height: 18px;
    float: left;
}
.title-day {
    font-size: 12px;
    font-weight: bold;
    width: 100%;
}
.title-day td {
    padding: 10px 0;
}
.information-ticket-new table tr td p {
    text-transform: capitalize;
    margin: 0;
}
.title-time td {
    padding: 5px;
}
.w12-5 {
    width: 12.5%;
}
.value {
    font-size: 18px;
    color: #716e75;
    font-weight: bold;
}
.sur td {
    padding: 7px 0;
    font-size: 12px;
}
.sur td p {
    text-align: left;
    padding-left: 10px;
    font-weight: bold;
    margin: 0;
}
.value td{
  padding: 10px;
}
.price-sub-table {
    float: left;
    width: 85%;
    text-align: left;
}
.price-sub-table li {
    float: left;
    width: 100%;
}
  .de {
      background: url(../img/icon/h3_theater.gif) no-repeat scroll center center / 204px 41px transparent;
      display: inline-block;
      width: 220px;
      margin-top: 5px;
      height: 41px;
      text-indent: -99999px;
  }
  
  .carousel-indicators li{
    display: none !important;
  }
  
  .tit1{
    padding-top: 60px;
  }
  .tabs ul{
    display: flex;
    justify-content: center;
  }
  .tabs{
    text-align: center;
    padding-top: 30px;
  }
  .tabs li a{
    background: url(../img/icon/bg-tebmenu-line.gif) no-repeat scroll right 16px #e71a0f;
    color: #fff;
    display: inline-block;
    padding: 12px;
    width: 150px;
  }
</style>