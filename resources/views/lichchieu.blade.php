@extends('layouts.default')
@section('content') 
<div class="container">
  <div class="descrip">
    <h2 class="de">Theater</h2>
  </div>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="{{asset('img/lamour-1.png')}}" style="width:100%;">
        <div class="carousel-caption">
          <h2>Giường nằm êm ái</h2>
          <p>Ghế ngồi là giường cùng chăn mang lại cảm giác ấm áp và tinh tế</p>
        </div>
      </div>

      <div class="item">
        <img src="{{asset('img/lamour-3.png')}}" style="width:100%;">
        <div class="carousel-caption">
          <h2>Giường nằm êm ái</h2>
          <p>Ghế ngồi là giường cùng chăn mang lại cảm giác ấm áp và tinh tế</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="tabs">
<ul>
  <li class="first" style="border-right: 1px solid white"><a href=""><strong>Lịch chiếu</strong></a></li>
  <li class="second"><a href="/giave">Giá vé</a></li>
</ul>
</div>
<div class="main1">
 <button class="tablink" onclick="openPage('chunhat')" id="defaultOpen">
    <p class="thu">Chủ nhật</p>
    <p>30/05</p>
  </button>
  <button class="tablink" onclick="openPage('thu2')">
    <p class="thu">Thứ 2</p>
    <p>31/05</p></button>
  <button class="tablink" onclick="openPage('thu3')">
    <p class="thu">Thứ 3</p>
    <p>01/06</p>
  </button>
  <button class="tablink" onclick="openPage('thu4')">
    <p class="thu">Thứ 4</p>
    <p>02/06</p>
  </button>
  <button class="tablink" onclick="openPage('thu5')">
    <p class="thu">Thứ 5</p>
    <p>03/06</p>
  </button>
  <button class="tablink" onclick="openPage('thu6')">
    <p class="thu">Thứ 6</p>
    <p>04/06</p>
  </button>
  <button class="tablink" onclick="openPage('thu7')">
    <p class="thu">Thứ 7</p>
    <p>05/06</p>
  </button>

  <div id="chunhat" class="tabcontent">
    <div class="row tgphim" style="padding-top: 30px">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">CROODS</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        <a href="/datve" class="btn-schedule">18:50</a>
        <a href="/datve" class="btn-schedule">19:50</a>
      </div>
    </div>
    <hr>
    <div class="row tgphim">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/frguy_teaser2_vietnam_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">Giải cứu "GUY"</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        <a href="/datve" class="btn-schedule">19:50</a>
      </div>
    </div>
  </div>

  <div id="thu2" class="tabcontent">
    <div class="row tgphim" style="padding-top: 30px">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/death_on_the_nile_teaser_vietnam_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">Án mạng trên sông Nile</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        <a href="/datve" class="btn-schedule">18:50</a>
        <a href="/datve" class="btn-schedule">19:50</a>
        <a href="/datve" class="btn-schedule">20:50</a>
      </div>
    </div>
  </div>

  <div id="thu7" class="tabcontent" >
    <div class="row tgphim" style="padding-top: 30px">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">CROODS</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        
        <a href="/datve" class="btn-schedule">19:50</a>
      </div>
    </div>
  </div>

  <div id="thu3" class="tabcontent">
    <div class="row tgphim" style="padding-top: 30px">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">CROODS</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        <a href="/datve" class="btn-schedule">18:50</a>
      </div>
    </div>
  </div>
  <div id="thu4" class="tabcontent">
    <div class="row tgphim" style="padding-top: 30px">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">CROODS</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        <a href="/datve" class="btn-schedule">18:50</a>
        <a href="/datve" class="btn-schedule">19:50</a>
      </div>
    </div>
  </div>
  <div id="thu5" class="tabcontent">
    <div class="row tgphim" style="padding-top: 30px">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">CROODS</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        <a href="/datve" class="btn-schedule">18:50</a>
        <a href="/datve" class="btn-schedule">19:50</a>
      </div>
    </div>
  </div>
  <div id="thu6" class="tabcontent">
    <div class="row tgphim" style="padding-top: 30px">
      <div class="col-md-4"  style="border-right: 1px solid black;">
        <div class="col-xs-4 col-sm-12 col-lg-5">
          <img src="{{asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg')}}">
        </div>
        <div class="col-xs-8 col-sm-12 col-lg-7 tit1">
          <p class="tit">CROODS</p>
        </div>
      </div>
      <div class="col-md-8 type2d">
        <a href="/datve" class="btn-schedule">18:50</a>
        <a href="/datve" class="btn-schedule">19:50</a>
      </div>
    </div>
  </div>
</div>
@endsection
<script type="text/javascript">
  window.onload = function () {
        startTab();
    };

    function startTab() {
        document.getElementById("defaultOpen").click();

    }
  function openPage(pageName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    
    document.getElementById(pageName).style.display = "block";
  }

</script>
<style type="text/css">
  .de {
      background: url(../img/icon/h3_theater.gif) no-repeat scroll center center / 204px 41px transparent;
      display: inline-block;
      width: 220px;
      margin-top: 5px;
      height: 41px;
      text-indent: -99999px;
  }
  
  .carousel-indicators li{
    display: none !important;
  }
 
  .main1{
    border: 1px solid black;
    margin-left: 200px;
    width: 983px;
    display: block;
  }
  .tablink {
    background-color: #555;
    color: white;
    float: left;
    border: 1px solid white;
    cursor: pointer;
    padding: 10px;
    font-size: 17px;
    width: 140px;
  }

  .tablink:hover {
    background-color: #777;
  }

  /* Style the tab content (and add height:100% for full page content) */
  .tabcontent {
    display: none;
    padding: 30px 20px;
  }
  .tabcontent img{
    width: 108px;
    height: 162px;
  }
  .type2d{
    background-image: url(/img/2d.png);
    background-position: left 15px center;
      background-repeat: no-repeat;
      padding: 60px 10px 10px 60px;
      margin: 10px 0px;
  }
  .btn-schedule{
    margin-left: 50px;
    padding: 10px;
    border: 1px solid black;
  }
  .tit1{
    padding-top: 60px;
  }
  .tabs ul{
    display: flex;
    justify-content: center;
  }
  .tabs{
    text-align: center;
    padding-top: 30px;
  }
  .tabs li a{
    background: url(../img/icon/bg-tebmenu-line.gif) no-repeat scroll right 16px #e71a0f;
    color: #fff;
    display: inline-block;
    padding: 12px;
    width: 150px;
  }

</style>