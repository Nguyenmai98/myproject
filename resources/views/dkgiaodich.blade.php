@extends('layouts.default')
@section('content')
	<div class="container">
		<h1 style="color: red">Điều khoản giao dịch</h1>
		<div class="main-section">
			<p style="margin-bottom: 10px; margin-top: 10px">Chào mừng quý khách đến với CGV</p>

			<p style="margin-bottom: 10px;">Chúng tôi là Công ty TNHH CJ CGV Vietnam, địa chỉ trụ sở chính tại Tòa nhà CJ, Tầng 10-11, 2Bis-4-6 đường Lê Thánh Tôn, Quận 1, Thành phố Hồ Chí Minh.</p>

			<p style="margin-bottom: 10px;">Khi quý khách truy cập vào trang web của CGV có nghĩa là quý khách đồng ý với các điều khoản này. Trang web có quyền thay đổi, chỉnh sửa, thêm hoặc lược bỏ bất kỳ phần nào trong Quy định và Điều kiện sử dụng, vào bất cứ lúc nào. Các thay đổi có hiệu lực ngay khi được đăng trên trang web mà không cần thông báo trước. Và khi quý khách tiếp tục sử dụng trang web, sau khi các thay đổi về Quy định và Điều kiện được đăng tải, có nghĩa là quý khách chấp nhận với những thay đổi đó. Quý khách vui lòng kiểm tra thường xuyên để cập nhật những thay đổi của CGV.</p>

			<p style="margin-bottom: 10px;">Xin vui lòng đọc kỹ trước khi quyết định Đặt Vé trực tuyến.</p>
			<p style="margin-bottom: 10px;"><b>1. Phạm Vi Áp Dụng</b></p>

			<p style="margin-bottom: 10px;">Điều kiện dưới đây áp dụng riêng cho chức năng mua vé trực tuyến tại Website. Khi sử dụng chức năng để đặt chỗ và mua vé, Quý khách mặc nhiên đã chấp thuận và tuân thủ tất cả các chỉ dẫn, điều khoản, điều kiện và lưu ý đăng tải trên Website, bao gồm nhưng không giới hạn bởi Điều kiện Sử dụng nêu ở đây. Nếu Quý khách không có ý định mua vé trực tuyến hay không đồng ý với bất kỳ điều khoản hay điều kiện nào nêu trong Điều kiện Sử dụng, xin hãy DỪNG VIỆC SỬ DỤNG chức năng này.</p>

			<p style="margin-bottom: 10px;"><b>2. Điều kiện sử dụng tính năng mua vé trực tuyến</b></p>

			<p style="margin-bottom: 10px;">Quý khách phải đăng ký tài khoản với thông tin xác thực về bản thân và phải cập nhật nếu có bất kỳ thay đổi nào. Mỗi người truy cập phải có trách nhiệm với mật khẩu, tài khoản và hoạt động của mình trên web. Hơn nữa, quý khách phải thông báo cho chúng tôi biết khi tài khoản bị truy cập trái phép. Chúng tôi không chịu bất kỳ trách nhiệm nào, dù trực tiếp hay gián tiếp, đối với những thiệt hại hoặc mất mát gây ra do quý khách không tuân thủ quy định.</p>
		</div>
	</div>
@endsection