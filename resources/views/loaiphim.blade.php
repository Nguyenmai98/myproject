@extends('layouts.default')
@section('content')	
<div class="container pdchieu">
	@foreach($categories as $category)
		<div class="phim col-sm-2">
			<a href="{{ route('chitietphim', $category->id) }}"><img src="{{$category->feature_image_path}}"></a>
			<div class="detail">
				<h3>{{$category->name}}</h3>
				<p><b>Thể loại: </b>{{$category->theloai}}</p>
				<p><b>Thời lượng: </b>{{$category->thoiluong}}</p>
				<p><b>Khởi chiếu: </b>{{$category->khoichieu}}</p>
			</div>
		</div>
	@endforeach
</div>
@endsection
<style type="text/css">
	.pdchieu{
		margin-left: 150px !important;
	}
	.phim img{
		height: 260px;
    	width: 185px;
    	border: 2px solid black;
	}
	.phim{
		margin-left: 15px;
		margin-right: 15px;
		padding-bottom: 5px;
	}
	.detail{
		width: 185px;
		margin-top: 10px;
	}
</style>