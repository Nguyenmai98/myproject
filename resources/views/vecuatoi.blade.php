@extends('layouts.default')
@section('content')
<div class="container">
    <h1 class="titlee">LỊCH SỬ GIAO DỊCH</h1>
    <div class="row">
      <div class="col-md-12">
            <table class="table" style="border: solid 2px #DDDDDD">
              <thead>
                <tr>
                  <th scope="col">STT</th>
                  <th scope="col">Phim</th>
                  <th scope="col">Ngày chiếu</th>
                  <th scope="col">Giờ chiếu</th>
                  <th scope="col">Phòng chiếu</th>
                  <th scope="col">Ghế</th>
                  <th scope="col">Tổng tiền</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <th><img src="{{ asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg') }}"></th>
                  <th>30/5/2021</th>
                  <th>18:50</th>
                  <th>P01</th>
                  <th>A3</th>
                  <th>50.000đ</th>
                </tr>
              </tbody>
            </table>
          </div>
    </div>
</div>
@endsection

<style type="text/css">
  .titlee{
    padding: 5px;
    background-color: black;
    color: white;
    text-align: center;
    font-size: 25px;
  }
  .table th{
    border-right: solid 2px #DDDDDD;
  }
</style>