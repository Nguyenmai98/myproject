@extends('layouts.default')
@section('content')
	<div class="container">
		<h1 style="color: red">Giới thiệu</h1>
		<div class="main-section">
			<p style="margin-bottom: 10px; margin-top: 10px"><b>1. Mục đích và phạm vi thu thập thông tin</b></p>

			<p style="margin-bottom: 10px;">Việc thu thập thông tin cá nhân được thực hiện trên cơ sở khách hàng tự khai báo để đăng ký thành viên CGV tại website www.cgv.vn, tùy từng thời điểm, thông tin thu thập sẽ bao gồm nhưng không giới hạn ở:</p>

			<p style="margin-bottom: 10px;">- Thông tin cá nhân như: họ tên, giới tính, độ tuổi, số CMND.</p>

			<p style="margin-bottom: 10px;">- Thông tin liên lạc như: địa chỉ, số điện thoại di động, email/fax.</p>

			<p style="margin-bottom: 10px;"><b>2. Phạm vi sử dụng thông tin</b></p>

			<p style="margin-bottom: 10px;">CGV chỉ sử dụng thông tin cá nhân của khách hàng cho các mục đích quy định tại Mục 1 hoặc mục đích khác (nếu có) với điều kiện đã thông báo và được sự đồng ý của khách hàng.</p>
		</div>
	</div>
@endsection