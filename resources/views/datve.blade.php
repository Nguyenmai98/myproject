@extends('layouts.default')
@section('content')
 <div class="container col1-layout">
	<div class="main-content">
	<label class="h1">Người / Ghế</label>

	<div class="ticketbox">
	<div class="screen"><span class="text-screen">Phòng chiếu</span></div>
		<div class="row row1">
			<div class="seat seat-standard active" id="changeIt" >A1</div>
			<div class="seat seat-standard active" id="changeIt" >A2</div>
			<div class="seat seat-standard active" id="changeIt" >A3</div>
			<div class="seat seat-standard active" id="changeIt" >A4</div>
			<div class="seat seat-standard active" id="changeIt" >A5</div>
			<div class="seat seat-standard active" id="changeIt" >A6</div>
			<div class="seat seat-standard active" id="changeIt" >A7</div>
			<div class="seat seat-standard active" id="changeIt" >A8</div>
			<div class="seat seat-standard active" id="changeIt" >A9</div>
			<div class="seat seat-standard active" id="changeIt" >A10</div>
			<div class="seat seat-standard active" id="changeIt" >A11</div>
			<div class="seat seat-standard active" id="changeIt" >A12</div>
			<div class="seat seat-standard active" id="changeIt" >A13</div>
			<div class="seat empty">Q88</div>
			<div class="seat seat-standard active" id="changeIt" >A14</div>
			<div class="seat seat-standard active" id="changeIt" >A15</div>
			<div class="seat seat-standard active" id="changeIt" >A16</div>
			<div class="seat seat-standard active" id="changeIt" >A17</div>
		</div>
		<div class="row row2">
			<div class="seat seat-standard active" id="changeIt" >B1</div>
			<div class="seat seat-standard active" id="changeIt" >B2</div>
			<div class="seat seat-standard active" id="changeIt" >B3</div>
			<div class="seat seat-standard active" id="changeIt" >B4</div>
			<div class="seat seat-standard active" id="changeIt" >B5</div>
			<div class="seat seat-standard active" id="changeIt" >B6</div>
			<div class="seat seat-standard active" id="changeIt" >B7</div>
			<div class="seat seat-standard active" id="changeIt" >B8</div>
			<div class="seat seat-standard active" id="changeIt" >B9</div>
			<div class="seat seat-standard active" id="changeIt" >B10</div>
			<div class="seat seat-standard active" id="changeIt" >B11</div>
			<div class="seat seat-standard active" id="changeIt" >B12</div>
			<div class="seat seat-standard active" id="changeIt" >B13</div>
			<div class="seat empty">Q88</div>
			<div class="seat seat-standard active" id="changeIt" >B14</div>
			<div class="seat seat-standard active" id="changeIt" >B15</div>
			<div class="seat seat-standard active" id="changeIt" >B16</div>
			<div class="seat seat-standard active" id="changeIt" >B17</div>
		</div>
		<div class="row row3">
			<div class="seat seat-standard active" id="changeIt" >C1</div>
			<div class="seat seat-standard active" id="changeIt" >C2</div>
			<div class="seat seat-standard active" id="changeIt" >C3</div>
			<div class="seat seat-standard active" id="changeIt" >C4</div>
			<div class="seat seat-standard active" id="changeIt" >C5</div>
			<div class="seat seat-standard active" id="changeIt" >C6</div>
			<div class="seat seat-standard active" id="changeIt" >C7</div>
			<div class="seat seat-standard active" id="changeIt" >C8</div>
			<div class="seat seat-standard active" id="changeIt" >C9</div>
			<div class="seat seat-standard active" id="changeIt" >C10</div>
			<div class="seat seat-standard active" id="changeIt" >C11</div>
			<div class="seat seat-standard active" id="changeIt" >C12</div>
			<div class="seat seat-standard active" id="changeIt" >C13</div>
			<div class="seat empty">Q88</div>
			<div class="seat seat-standard active" id="changeIt" >C14</div>
			<div class="seat seat-standard active" id="changeIt" >C15</div>
			<div class="seat seat-standard active" id="changeIt" >C16</div>
			<div class="seat seat-standard active" id="changeIt" >C17</div>
		</div>
		<div class="row row4">
			<div class="seat seat-standard active" id="changeIt" >D1</div>
			<div class="seat seat-standard active" id="changeIt" >D2</div>
			<div class="seat seat-standard active" id="changeIt" >D3</div>
			<div class="seat seat-standard active" id="changeIt" >D4</div>
			<div class="seat seat-standard active" id="changeIt" >D5</div>
			<div class="seat seat-standard active" id="changeIt" >D6</div>
			<div class="seat seat-standard active" id="changeIt" >D7</div>
			<div class="seat seat-standard active" id="changeIt" >D8</div>
			<div class="seat seat-standard active" id="changeIt" >D9</div>
			<div class="seat seat-standard active" id="changeIt" >D10</div>
			<div class="seat seat-standard active" id="changeIt" >D11</div>
			<div class="seat seat-standard active" id="changeIt" >D12</div>
			<div class="seat seat-standard active" id="changeIt" >D13</div>
			<div class="seat empty">Q88</div>
			<div class="seat seat-standard active" id="changeIt" >D14</div>
			<div class="seat seat-standard active" id="changeIt" >D15</div>
			<div class="seat seat-standard active" id="changeIt" >D16</div>
			<div class="seat seat-standard active" id="changeIt" >D17</div>
		</div>
		<div class="row row5">
			<div class="seat seat-standard active" id="changeIt" >E1</div>
			<div class="seat seat-standard active" id="changeIt" >E2</div>
			<div class="seat seat-standard active" id="changeIt" >E3</div>
			<div class="seat seat-standard active" id="changeIt" >E4</div>
			<div class="seat seat-standard active" id="changeIt" >E5</div>
			<div class="seat seat-standard active" id="changeIt" >E6</div>
			<div class="seat seat-standard active" id="changeIt" >E7</div>
			<div class="seat seat-standard active" id="changeIt" >E8</div>
			<div class="seat seat-standard active" id="changeIt" >E9</div>
			<div class="seat seat-standard active" id="changeIt" >E10</div>
			<div class="seat seat-standard active" id="changeIt" >E11</div>
			<div class="seat seat-standard active" id="changeIt" >E12</div>
			<div class="seat seat-standard active" id="changeIt" >E13</div>
			<div class="seat empty">Q88</div>
			<div class="seat seat-standard active" id="changeIt" >E14</div>
			<div class="seat seat-standard active" id="changeIt" >E15</div>
			<div class="seat seat-standard active" id="changeIt" >E16</div>
			<div class="seat seat-standard active" id="changeIt" >E17</div>
		</div>
		<div class="row row6">
			<div class="seat seat-standard active" id="changeIt" >G1</div>
			<div class="seat seat-standard active" id="changeIt" >G2</div>
			<div class="seat seat-standard active" id="changeIt" >G3</div>
			<div class="seat seat-standard active" id="changeIt" >G4</div>
			<div class="seat seat-standard active" id="changeIt" >G5</div>
			<div class="seat seat-standard active" id="changeIt" >G6</div>
			<div class="seat seat-standard active" id="changeIt" >G7</div>
			<div class="seat seat-standard active" id="changeIt" >G8</div>
			<div class="seat seat-standard active" id="changeIt" >G9</div>
			<div class="seat seat-standard active" id="changeIt" >G10</div>
			<div class="seat seat-standard active" id="changeIt" >G11</div>
			<div class="seat seat-standard active" style="background: #bbb;" id="changeIt" >G12</div>
			<div class="seat seat-standard active" id="changeIt" >G13</div>
			<div class="seat empty">Q88</div>
			<div class="seat seat-standard active" id="changeIt" >G14</div>
			<div class="seat seat-standard active" id="changeIt" >G15</div>
			<div class="seat seat-standard active" id="changeIt" >G16</div>
			<div class="seat seat-standard active" style="background: #bbb; color: white;" id="changeIt" >X</div>
		</div>
		<div class="row row7">
			<div class="seat seat-vipprime active" id="changeIt" >H1</div>
			<div class="seat seat-vipprime active" id="changeIt" >H2</div>
			<div class="seat seat-vipprime active" id="changeIt" >H3</div>
			<div class="seat seat-vipprime active" id="changeIt" >H4</div>
			<div class="seat seat-vipprime active" id="changeIt" >H5</div>
			<div class="seat seat-vipprime active" id="changeIt" >H6</div>
			<div class="seat seat-vipprime active" id="changeIt" >H7</div>
			<div class="seat seat-vipprime active" id="changeIt" >H8</div>
			<div class="seat seat-vipprime active" id="changeIt" >H9</div>
			<div class="seat seat-vipprime active" id="changeIt" >H10</div>
			<div class="seat seat-vipprime active" id="changeIt" >H11</div>
			<div class="seat seat-vipprime active" id="changeIt" >H12</div>
			<div class="seat seat-vipprime active" id="changeIt" >H13</div>
			<div class="seat empty">Q88</div>
			<div class="seat seat-vipprime active" id="changeIt" >H14</div>
			<div class="seat seat-vipprime active" id="changeIt" >H15</div>
			<div class="seat seat-vipprime active" id="changeIt" >H16</div>
			<div class="seat seat-vipprime active" id="changeIt" >H17</div>
		</div>
		
		<div class="ticketbox-notice" style="padding-top: 10px;">
			<div class="iconlist">
				<div class="icon checked">Checked</div>
				<div class="icon occupied">Đã chọn</div>
				<div class="icon unavailable">Không thể chọn</div>
			</div>
			<div class="iconlist">
				<div class="icon zone-standard" title="Standard">Thường</div>
					<div class="icon zone-vipprime" title="VIP(Prime)">VIP(Prime)</div>
			</div>
		</div>
	</div>
	<div class="bottom-content">
		<a class="btn-left" href="" title="Previous"></a>
		<div class="minicart-wrapper">
			<ul>
				<li class="item first" xmlns="http://www.w3.org/1999/html">
					<div class="product-details">
						<table class="info-wrapper">
							<colgroup>
								<col width="40%"/>
								<col/>
							</colgroup>
							<tbody>
								<tr>
									<td>
										<img src="{{ asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg') }}" alt="CROODS" />
									</td>									
									<td>
										<table class="info-wrapper" style="max-width: 100px">
											<tr><td class="label">CROODS</td></tr>
											<tr><td class="label">2D</td></tr>
											<tr><td class="label">C14</td></tr>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</li>
			
				<li class="item" xmlns="http://www.w3.org/1999/html">
					<div class="product-details">
						<table class="info-wrapper">							
							<tbody>
								<tr>
									<td class="label">Suất chiếu</td>
									<td>
									18:50, 30/05/2021</td>
								</tr>
								<tr>
									<td class="label">Phòng chiếu</td>
									<td>P01</td>
								</tr>
								<tr class="block-seats">
									<td class="label">Ghế</td>
									<td class="data">A3</td>
								</tr>
							</tbody>
						</table>
					</div>
				</li>
				
				<li class="item" xmlns="http://www.w3.org/1999/html">
					<div class="product-details">
						<table class="info-wrapper">
							<thead>
								<tr class="block-box">
									<td class="label">Tên phim</td>
									<td class="price"><span class="price">50.000 ₫</span></td>
									
								</tr>
							</thead>
							<tfoot class="block-price">
								<tr>
									<td class="label">Tổng</td>
									<td class="price" colspan="2"><span class="price">50.000 ₫</span></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</li>
			</ul>
		</div>
		
		<a class="btn-right box" href="/thanhtoan" title="Next"></a>
		
	</div>
</div>
</div>
</div>
<script type="text/javascript">
	var list = document.querySelector('.row1');
	list.addEventListener('click', function(ev) {
	  if (ev.target.tagName === 'DIV') {
	    ev.target.classList.toggle('checked');
	  }
	}, false);
	var list = document.querySelector('.row2');
	list.addEventListener('click', function(ev) {
	  if (ev.target.tagName === 'DIV') {
	    ev.target.classList.toggle('checked');
	  }
	}, false);
	var list = document.querySelector('.row3');
	list.addEventListener('click', function(ev) {
	  if (ev.target.tagName === 'DIV') {
	    ev.target.classList.toggle('checked');
	  }
	}, false);
	var list = document.querySelector('.row4');
	list.addEventListener('click', function(ev) {
	  if (ev.target.tagName === 'DIV') {
	    ev.target.classList.toggle('checked');
	  }
	}, false);
	var list = document.querySelector('.row5');
	list.addEventListener('click', function(ev) {
	  if (ev.target.tagName === 'DIV') {
	    ev.target.classList.toggle('checked');
	  }
	}, false);
	var list = document.querySelector('.row6');
	list.addEventListener('click', function(ev) {
	  if (ev.target.tagName === 'DIV') {
	    ev.target.classList.toggle('checked');
	  }
	}, false);
	var list = document.querySelector('.row7');
	list.addEventListener('click', function(ev) {
	  if (ev.target.tagName === 'DIV') {
	    ev.target.classList.toggle('checked');
	  }
	}, false);
</script>
@endsection
<style type="text/css">
	.main-content .h1 {
	    text-align: center;
	    width: 100%;
	    background: #bcbdc0;
	    font-size: 16px;
	    line-height: 32px;
	    border-top: 2px solid #d4d3c9;
	    border-bottom: 2px solid #d4d3c9;
	}
	.main-content {
	    white-space: nowrap;
	    overflow: hidden;
	    border: 2px solid #d4d3c9;
	    width: 100%;
	}
	.screen {
	    width: 100%;
	    height: 45px;
	    margin: 20px auto 40px;
	    text-align: center;
	    background: url(../img/icon/bg-screen.png) no-repeat top center transparent;
	    background-size: 100% auto;
	}
	.text-screen {
	    display: none;
	}
	.ticketbox {
	    overflow-x: auto;
	    text-align: center;
	}
	.seat {
	    display: inline-block;
	    width: 22px;
	    height: 22px;
	    vertical-align: middle;
	    font-size: 7pt;
	    margin: 0 1px;
	    padding: 1px 0;
	    text-align: center;
	}
	.seat.checked{
		background: #B11500;
	}
	.empty {
	    text-indent: -99999px;
	    border: none;
	}
	.seat.active {
	    cursor: pointer;
	}
	.seat-standard {
	    border: 1px solid #01c73c;
	}
	.seat-vipprime {
	    border: 1px solid #f71708;
	}
	.row{
		display: inline !important;
	}
	.ticketbox-notice {
	    overflow: hidden;
	    text-align: center;
	}
	.iconlist {
	    display: inline-block;
	    min-width: 25%;
	    vertical-align: top;
	}
	.icon {
	    text-align: left;
	    margin: 5px 0;
	}
	.icon.checked:before {
	    background: #b11500;
	}
	.icon:before {
	    content: "";
	    display: inline-block;
	    vertical-align: middle;
	    width: 18px;
	    height: 18px;
	    margin: 0 5px 0 0;
	}
	.icon.occupied:before {
	    background: #bbb;
	}
	.icon.unavailable:before {
	    background: #bbb;
	    content: "X";
	    text-align: center;
	    color: #fff;
	}
	.icon.zone-standard:before {
	    border: 1px solid #01c73c;
	}
	.icon.zone-vipprime:before {
	    border: 1px solid #f71708;
	}
	.bottom-content {
	    background-color: #000;
	    margin-top: 30px;
	    font-size: 0;
	}
	.btn-left, .btn-right {
	    float: none;
	    width: 106px;
	    display: inline-block;
	    margin: 0 0 0 3px;
	    height: 108px;
	    vertical-align: top;
	    background: url(/img/icon/bg-cgv-button-process.png) no-repeat;
	    margin-top: 20px;
	}
	.btn-left {
	    background-position: -152px 0;
	    margin-left: 10px;
	}

	.btn-right {
	    background-position: -152px -441px;
	    float: right;
	    margin-right: 10px;
	}
	.minicart-wrapper {
	    font-size: 0;
	    color: #cccccc;
	    width: 750px;
	    display: inline-block;
	    margin: 0 3px;
	}
	.minicart-wrapper .item {
	    display: inline-block;
	    width: 32%;
	    margin-left: 1%;
	    font-size: 12px;
	}
	.minicart-wrapper ul .item.first .product-details {
	    margin: 0px;
	}

	.minicart-wrapper .product-details {
	    padding-top: 10px;
	    display: inline-block;
	    vertical-align: top;
	    margin: 0 0 0 10px;
	}
	.info-wrapper {
	    color: #cccccc;
	}
	.info-wrapper td {
		padding: 0 5px;
		white-space: normal;
	    font-weight: bold;
	    vertical-align: middle;
	}
	.label{
		line-height: 2 !important;
	}
	.minicart-wrapper img {
	    width: 74px;
	    height: 108px;
	    display: inline-block;
	}
	.minicart-wrapper .product-details {
	    padding-top: 20px;
	    display: inline-block;
	    vertical-align: top;
	    margin: 0 0 0 10px;
	}
</style>