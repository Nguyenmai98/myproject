@extends('layouts.default')
@section('content')
	<div class="container">
		<h1 style="color: red">Giới thiệu</h1>
		<div class="main-section">
			
			<p style="margin-bottom: 10px; margin-top: 10px"><b>1. Quy định về thanh toán</b> </p>

			<p style="margin-bottom: 10px;">Khách hàng có thể lựa chọn các hình thức thanh toán sau để thanh toán cho giao dịch đặt vé trên website CGV</p>

			<p style="margin-bottom: 10px;">- Điểm thưởng thành viên</p>

			<p style="margin-bottom: 10px;">- Thẻ quà tặng CGV (CGV Giftcard)</p>

			<p style="margin-bottom: 10px;">- Ticket Voucher</p>

			<p style="margin-bottom: 10px;">- Thanh toán bằng Ví điện tử MoMo</p>

			<p style="margin-bottom: 10px;"><b>2. Chi tiết các hình thức thanh toán</b></p>

			<p style="margin-bottom: 10px;">Điểm Thưởng Thành Viên (Membership Point): Mỗi 01 điểm thưởng tương đương với 1.000 VND. Điểm thưởng này, bạn có thể sử dụng để thanh toán vé xem phim và các sản phẩm đồ ăn thức uống tại hệ thống CGV toàn quốc. Khi sử dụng điểm thưởng, bạn vui lòng xuất trình thẻ thành viên để được nhân viên hỗ trợ thanh toán. Điểm thưởng được sử dụng phải từ 20 điểm trở lên.</p>
		</div>
	</div>
@endsection