@extends('layouts.default')
@section('content')
<div class="container">
	<div class="page-title">
		<h1>THANH TOÁN</h1>
	</div>
	<div class="row">
	<div class="col-md-9">
		<h2 style="background-color: #D0CECE; font-size: 20px; padding: 10px; font-weight: bold;">HÌNH THỨC THANH TOÁN</h2>
		<div class="mainthanhtoan" style="background-color: #FFF1CE; padding: 15px;">
		<div class="thanhtoan">
			<input type="radio" name="methodtt" style="vertical-align: top;">
			<span class="pm_icon" style="background-image:url('https://www.cgv.vn/media/catalog/product/placeholder/default/atm_icon.png')"></span>
			<span class="pm_name method_label">ATM card (Thẻ nội địa)</span>
		</div>
		<br>
		<div class="thanhtoan">
			<input type="radio" name="methodtt" style="vertical-align: top;">
			<span class="pm_icon" style="background-image:url('https://www.cgv.vn/media/catalog/product/placeholder/default/visa-mastercard-icon.png')"></span>
			<span class="pm_name method_label">Visa Mastercard</span>
		</div>
		<br>
		<div class="thanhtoan">
			<input type="radio" name="methodtt" style="vertical-align: top;">
			<span class="pm_icon" style="background-image:url('https://www.cgv.vn/media/catalog/product/placeholder/default/momo_icon.png')"></span>
			<span class="pm_name method_label">Momo</span>
		</div>
		<br>
		<div class="thanhtoan">
			<input type="radio" name="methodtt" style="vertical-align: top;">
			<span class="pm_icon" style="background-image:url('https://www.cgv.vn/media/catalog/product/placeholder/default/zalopay_icon.png')"></span>
			<span class="pm_name method_label">Zalopay</span>
		</div>
		<br>
		<div class="thanhtoan">
			<input type="radio" name="methodtt" style="vertical-align: top;">
			<span class="pm_icon" style="background-image:url('https://www.cgv.vn/media/catalog/product/placeholder/default/viettelpay-logo.jpg')"></span>
			<span class="pm_name method_label">Viettelpay</span>
		</div>
	</div>
	</div>
	<div class="col-md-3">
		<h2 style="background-color: #E71A0F; font-size: 20px; padding: 10px; font-weight: bold; color: white; text-align: center">TỔNG THANH TOÁN</h2>
		<p style="background-color: #D0CECE; font-size: 14px; padding: 10px; font-weight: bold; text-align: center;">50 000đ</p>
	</div>
	</div>
	<div class="bottom-content">
		<a class="btn-left" href="" title="Previous"></a>
		<div class="minicart-wrapper">
			<ul>
				<li class="item first" xmlns="http://www.w3.org/1999/html">
					<div class="product-details">
						<table class="info-wrapper">
							<colgroup>
								<col width="40%"/>
								<col/>
							</colgroup>
							<tbody>
								<tr>
									<td>
										<img src="{{ asset('img/phim/cr2_digtal_1sheet_teaser_evolution_vie_1.jpg') }}" alt="CROODS" />
									</td>									
									<td>
										<table class="info-wrapper" style="max-width: 100px">
											<tr><td class="label">CROODS</td></tr>
											<tr><td class="label">2D</td></tr>
											<tr><td class="label">C14</td></tr>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</li>
			
				<li class="item" xmlns="http://www.w3.org/1999/html">
					<div class="product-details">
						<table class="info-wrapper">							
							<tbody>
								<tr>
									<td class="label">Suất chiếu</td>
									<td>
									18:50, 30/05/2021</td>
								</tr>
								<tr>
									<td class="label">Phòng chiếu</td>
									<td>P01</td>
								</tr>
								<tr class="block-seats">
									<td class="label">Ghế</td>
									<td class="data">A3</td>
								</tr>
							</tbody>
						</table>
					</div>
				</li>
				
				<li class="item" xmlns="http://www.w3.org/1999/html">
					<div class="product-details">
						<table class="info-wrapper">
							<thead>
								<tr class="block-box">
									<td class="label">Tên phim</td>
									<td class="price"><span class="price">50.000 ₫</span></td>
									
								</tr>
							</thead>
							<tfoot class="block-price">
								<tr>
									<td class="label">Tổng</td>
									<td class="price" colspan="2"><span class="price">50.000 ₫</span></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</li>
			</ul>
		</div>
		
		<a class="btn-right box" href="/thanhtoan" title="Next"></a>
		
	</div>
</div>
@endsection
<style type="text/css">
	.page-title h1{
		padding: 10px;
		background-color: black;
		text-align: center;
		font-size: 22px;
		color: white;
	}
	.bottom-content {
    background-color: #000;
    margin-top: 30px;
    font-size: 0;
}
.btn-left, .btn-right {
    float: none;
    width: 106px;
    display: inline-block;
    margin: 0 0 0 3px;
    height: 108px;
    vertical-align: top;
    background: url(/img/icon/bg-cgv-button-process.png) no-repeat;
    margin-top: 20px;
}
.btn-left {
    background-position: -152px 0;
    margin-left: 10px;
}

.btn-right {
    background-position: -151px -110px;
    float: right;
    margin-right: 10px;
    height: 108px;
}
.minicart-wrapper {
    font-size: 0;
    color: #cccccc;
    width: 750px;
    display: inline-block;
    margin: 0 3px;
}
.minicart-wrapper .item {
    display: inline-block;
    width: 32%;
    margin-left: 1%;
    font-size: 12px;
}
.minicart-wrapper ul .item.first .product-details {
    margin: 0px;
}

.minicart-wrapper .product-details {
    padding-top: 10px;
    display: inline-block;
    vertical-align: top;
    margin: 0 0 0 10px;
}
.info-wrapper {
    color: #cccccc;
}
.info-wrapper td {
    padding: 0 5px;
	white-space: normal;
    font-weight: bold;
    vertical-align: middle;
}
.label{
	line-height: 2 !important;
}
.minicart-wrapper img {
    width: 74px;
    height: 108px;
    display: inline-block;
}
.minicart-wrapper .product-details {
    padding-top: 20px;
    display: inline-block;
    vertical-align: top;
    margin: 0 0 0 10px;
}
.pm_icon {
    background-position: 50% 50%;
    background-size: 100% 100%;
    width: 37px;
    height: 37px;
    margin-right: 5px;
    margin-top: -9px;
    display: inline-block;
}
.pm_name {
    margin-top: -31px;
    vertical-align: middle;
    display: inline-block;
}
</style>