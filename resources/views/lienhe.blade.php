@extends('layouts.default')
@section('content')
	<div class="container">
		<h1 style="color: red">Liên hệ CGV</h1>
		<div class="main-section">
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25615.906364793133!2d106.70663701731654!3d10.78025381605616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf16ffaa20e46e87f!2sCJ+CGV+Vietnam!5e0!3m2!1sen!2s!4v1459915713967" width="500" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
			</div>
			<div class="col-md-6">
			<form action="" method="post"> 
          		@csrf
				  <p><b>Tên <span style="color: red">*</span></b></p>
				  	<input class="input" type="text" name="fullname">

				  <p><b>Số điện thoại <span style="color: red">*</span></b></p>
				  	<input class="input" type="text" name="sdt">

				  <p><b>Email <span style="color: red">*</span></b></p>
				  	<input class="input" type="email" name="email">

				  <p><b>Nội dung cần liên hệ <span style="color: red">*</span></b></p>
				  	<textarea class="input input1" name="content" cols="50"></textarea>
			</form>
			<button class="bton">GỬI ĐI</button>
			</div>
		</div>
	</div>
@endsection
<style type="text/css">
	.input{
		width: 500px;
		margin-bottom: 10px;
	}
	.input1{
		height: 200px;
	}
	.bton{
		width: 80px;
		height: 40px;
		background-color: red;
		color: white;
		border: 0.5px solid white;
		border-radius: 10px;
	}
</style>