@extends('layouts.default')
@section('content')
	<div class="container">
		<h1 style="color: red">Điều khoản chung</h1>
		<div class="main-section">
			<p style="margin-bottom: 10px; margin-top: 10px">Việc bạn sử dụng website này đồng nghĩa với việc bạn đồng ý với những thỏa thuận dưới đây.Nếu bạn không đồng ý, xin vui lòng không sử dụng website.</p>

			<p style="margin-bottom: 10px;"><b>1. Trách nhiệm của người sử dụng:</b></p>

			<p style="margin-bottom: 10px;">Khi truy cập vào trang web này, bạn đồng ý chấp nhận mọi rủi ro. CGV và các bên đối tác khác không chịu trách nhiệm về bất kỳ tổn thất nào do những hậu quả trực tiếp, tình cờ hay gián tiếp; những thất thoát, chi phí (bao gồm chi phí pháp lý, chi phí tư vấn hoặc các khoản chi tiêu khác) có thể phát sinh trực tiếp hoặc gián tiếp do việc truy cập trang web hoặc khi tải dữ liệu về máy; những tổn hại gặp phải do virus, hành động phá hoại trực tiếp hay gián tiếp của hệ thống máy tính khác, đường dây điện thoại, phần cứng, phần mềm, lỗi chương trình, hoặc bất kì các lỗi nào khác; đường truyền dẫn của máy tính hoặc nối kết mạng bị chậm…</p>

			<p style="margin-bottom: 10px;"><b>2. Về nội dung trên trang web:</b></p>

			<p style="margin-bottom: 10px;">Tất cả những thông tin ở đây được cung cấp cho bạn một cách trung thực như bản thân sự việc. CGV và các bên liên quan không bảo đảm, hay có bất kỳ tuyên bố nào liên quan đến tính chính xác, tin cậy của việc sử dụng hay kết quả của việc sử dụng nội dung trên trang web này. Nội dung trên website được cung cấp vì lợi ích của cộng đồng và có tính phi thương mại. Các cá nhân và tổ chức không được phép sử dụng nội dung trên website này với mục đích thương mại mà không có sự ưng thuận của CGV bằng văn bản. Mặc dù CGV luôn cố gắng cập nhật thường xuyên các nội dung tại trang web, nhưng chúng tôi không bảo đảm rằng các thông tin đó là mới nhất, chính xác hay đầy đủ. Tất cả các nội dung website có thể được thay đổi bất kỳ lúc nào.</p>

			<p style="margin-bottom: 10px;"><b>3. Về bản quyền:</b></p>

			<p style="margin-bottom: 10px;">CGV là chủ bản quyền của trang web này. Việc chỉnh sửa trang, nội dung, và sắp xếp thuộc về thẩm quyền của CGV. Sự chỉnh sửa, thay đổi, phân phối hoặc tái sử dụng những nội dung trong trang này vì bất kì mục đích nào khác được xem như vi phạm quyền lợi hợp pháp của CGV.</p>
		</div>
	</div>
@endsection