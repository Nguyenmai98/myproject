@extends('layouts.default')
@section('content')	
<div class="container login">
	<div class="col-md-12">
		<div class="col-md-6">
			<button class="tablink" onclick="openPage('Login')" id="defaultOpen">Đăng nhập</button>
			<button class="tablink" onclick="openPage('Signup')">Đăng ký</button>
			<div id="Login" class="tabcontent">
				<form id="login-form" class="form" action="" method="post">
                  @csrf
				  <p style="padding-top: 45px"><b>Email hoặc số điện thoại</b></p>
				  <input type="text" name="email" placeholder="Email hoặc số điện thoại">
				  <p><b>Mật khẩu</b></p>
				  <input type="password" name="password" placeholder="Mật khẩu" style="margin-bottom: 20px;">
				  <input type="submit" class="submit" value="Đăng nhập">
				  <p><a href="" style="text-decoration: underline;">Bạn muốn tìm lại mật khẩu?</a></p>
				</form>
			</div>

			<div id="Signup" class="tabcontent">
				<form action="{{ route('members.store') }}" method="post"> 
          		@csrf
				  <p style="padding-top: 45px"><b>Tên <span style="color: red">*</span></b></p>
				  	<input type="text" name="fullname" placeholder="Tên">

				  <p><b>Số điện thoại <span style="color: red">*</span></b></p>
				  	<input type="text" name="sdt" placeholder="Số điện thoại">

				  <p><b>Email <span style="color: red">*</span></b></p>
				  	<input type="email" name="email" placeholder="Email">

				  <p><b>Mật khẩu <span style="color: red">*</span></b></p>
				  	<input type="password" name="password" placeholder="Mật khẩu">

				  <p><b>Ngày sinh <span style="color: red">*</span></b></p>
				  <input type="text" name="ngaysinh" placeholder="Ngày sinh">

	              <p><b>Giới tính <span style="color: red">*</span></b></p>
	              <label class="radio-inline" style="margin-left: 20px; margin-right: 10px">
				       {!! Form::radio('gioitinh', "Nam", null,['required']) !!} Nam
				   </label>
				   <label class="radio-inline">
				        {!! Form::radio('gioitinh', "Nu", null) !!} Nữ
				    </label>

				  <br>
				  <input type="submit" class="submit" value="Đăng ký">
				</form>
			</div>
		</div>
		<script>
		function openPage(pageName) {
		  var i, tabcontent, tablinks;
		  tabcontent = document.getElementsByClassName("tabcontent");
		  for (i = 0; i < tabcontent.length; i++) {
		    tabcontent[i].style.display = "none";
		  }
		  tablinks = document.getElementsByClassName("tablink");
		  
		  document.getElementById(pageName).style.display = "block";
		}

		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
		<div class="col-md-6">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			  <ol class="carousel-indicators">
			    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			  </ol>
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <img class="d-block w-100" src="img/dangnhap/1.jpg" alt="First slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="img/dangnhap/2.jpg" alt="Second slide">
			    </div>
			  </div>
			  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
	</div>
</div>
@endsection
<style type="text/css">
	.capcha{
		position: relative;
	}
	.capcha img{
		margin: 10px 0px 10px 0px;
	}
	.capcha-reload {
	    position: absolute;
	}
	.capcha-content {
	    border: 1px solid #b6b6b6;
	}
	* {box-sizing: border-box}

	/* Set height of body and the document to 100% */
	body, html {
	  height: 100%;
	  margin: 0;
	  font-family: Arial;
	}

	/* Style tab links */
	.tablink {
	  background-color: red;
	  color: white;
	  float: left;
	  border: none;
	  outline: none;
	  cursor: pointer;
	  padding: 14px 16px;
	  font-size: 17px;
	  width: 50%;
	}

	.tablink:hover {
	  background-color: #777;
	}

	/* Style the tab content (and add height:100% for full page content) */
	.tabcontent {
	  display: none;
	  padding-top: 15px;
	}
	.submit{
		background-color: red;
		width: 100%;
		color: white;
		font-size: 17px;
		padding: 14px 16px;
	}
	input[type="text"], input[type="password"], input[type="email"] {
       	font-size:15px;
       	width: 100%;
	    padding: 10px;
	    margin: 8px 0;
	    display: inline-block;
	    border: 1px solid #ccc;
	    border-radius: 2px;
	    box-sizing: border-box;
	}
	.login p, .login label{
		padding: 10px 0px 10px 0px;
		font-size: 15px;
	}
	select{
		width: 20%;
		padding: 10px;
	    margin: 8px 0;
	    display: inline-block;
	    border: 1px solid #ccc;
	    border-radius: 2px;
	    box-sizing: border-box;
	}
	select, option{
		font-size: 14px;
	}
</style>