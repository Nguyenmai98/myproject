@extends('layouts.default')
@section('content')	
	<div class="container">
		<div class="memtit">
			<h1>CGV MEMBERSHIP</h1>
		</div>
		<div class="col-md-12">
			<div class="quyenloi">
				<ul>
					<li class="active">
						<button class="tablink" onclick="openPage('ctdiemthuong')" 
						id="defaultOpen">CHƯƠNG TRÌNH ĐIỂM THƯỞNG</button>
					</li>
					<li>
						<button class="tablink" onclick="openPage('qtsinhnhat')">QUÀ TẶNG SINH NHẬT</button>
					</li>
					<li>
						<button class="tablink" onclick="openPage('cdthanhvien')">CẤP ĐỘ THÀNH VIÊN</button>
					</li>
					<li>
						<button class="tablink" onclick="openPage('qltaikhoan')">QUẢN LÝ TÀI KHOẢN</button>
					</li>
					<li>
						<button class="tablink" onclick="openPage('bchotro')">BẠN CẦN HỖ TRỢ</button>
					</li>
				</ul>
				<div class="tabcontent" id="ctdiemthuong">
					<div class="tit">CHƯƠNG TRÌNH ĐIỂM THƯỞNG</div>
					<p>Chương trình bao gồm 4 đối tượng thành viên <strong>U22 | CGV Member | CGV VIP và CGV VVIP</strong>, với những quyền lợi và mức ưu đãi khác nhau. Mỗi khi thực hiện giao dịch tại hệ thống rạp CGV, bạn sẽ nhận được một số điểm thưởng tương ứng với cấp độ thành viên:</p>					
				</div>
				<div class="tabcontent" id="qtsinhnhat">
					<div class="tit">QUÀ TẶNG SINH NHẬT</div>
					<p>Như một lời chúc ý nghĩa và chân thành, CGV xin dành tặng bạn một phần quà đặc biệt nhân dịp sinh nhật của mình:</p>

					<p><strong>- Thành viên U22</strong>: 01 CGV Combo (1 Bắp & 2 Nước) miễn phí.</p>

					<p>* Đặc biệt! Vào sinh nhật lần thứ 23, thành viên U22 sẽ nhận thêm 01 vé xem phim 2D/3D miễn phí.</p>
					<p><strong>- Thành viên Thân Thiết (Member)</strong>: 01 CGV Combo (1 Bắp & 2 Nước) miễn phí.</p>
				</div>
				<div class="tabcontent" id="cdthanhvien">
					<div class="tit">THÀNH VIÊN U22</div>
					  	<p>Thành viên U22 là thành viên trong độ tuổi từ 12 đến dưới 23 và nhận được các ưu đãi sau:</p>

						<p>	- Giá vé ưu đãi 45.000 VND - 55.000VND/ vé 2D(*) cho tất cả các suất chiếu từ thứ 2 đến thứ 6 (Không áp dụng cho Lễ/ Tết, Sneakshow hoặc Early Release)</p>
					<div class="tit">THÀNH VIÊN THÂN THIẾT</div>
					  	<p>Thành viên thân thiết là thành viên từ 23 tuổi trở lên và nhận được các ưu đãi sau:</p>

						<p>- 01 CGV Combo (1 bắp & 2 nước) miễn phí vào tháng sinh nhật.</p>

					<div class="tit">THÀNH VIÊN VIP</div>
					  	<p>Thành viên VIP 2020 là thành viên thân thiết hoặc thành viên U22 có tổng chi tiêu trong năm 2019 từ 5.500.000 VND đến 9.999.999 VND và nhận được các ưu đãi đặc biệt sau:</p>

						<p>- 08 vé 2D/3D miễn phí áp dụng tất cả các ngày trong tuần.</p>
					
				</div>
				<div class="tabcontent" id="qltaikhoan">
					<div class="tit">QUẢN LÝ TÀI KHOẢN</div>
					<p>Đăng nhập vào Tài Khoản CGV, bạn có thể dễ dàng quản lý tài khoản thành viên của mình như:</p>

					<p>- Kiểm tra và chỉnh sửa thông tin tài khoản.</p>
				</div>
				<div class="tabcontent" id="bchotro">
					<div class="tit">BẠN CẦN HỖ TRỢ</div>
					<p>Với những ưu đãi hấp dẫn từ chương trình thành viên, CGV hy vọng sẽ mang đến cho bạn những trải nghiệm vượt xa điện ảnh.</p>

					<p>Mọi thắc mắc về chương trình thành viên bạn có thể liên hệ ngay Bộ phận hỗ trợ khách hàng của chúng tôi qua email hoidap@cgv.vn hoặc hotline 1900 6017 (8:00 – 22:00, từ thứ Hai đến Chủ Nhật - bao gồm các ngày Lễ, Tết).</p>
				</div>
			</div>
		</div>
	</div>
@endsection
<script>
window.onload = function () {
        startTab();
    };

    function startTab() {
        document.getElementById("defaultOpen").click();

    }
	function openPage(pageName) {
	  var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablink");
	  
	  document.getElementById(pageName).style.display = "block";
	}
</script>
<style type="text/css">
	.memtit h1{
		background: url(img/icon/member-title.png) no-repeat scroll center center / 465px auto transparent;
		font-size: 0;
    	height: 65px;
    	margin: 0;
	}
	.memtit{
		background: url(../img/icon/member-star.PNG) no-repeat scroll center bottom / 320px auto transparent;
	    float: left;
	    height: 65px;
	    margin-bottom: 30px;
	    padding-bottom: 100px;
	    text-align: center;
	    width: 100%;
	}
	.tablink{
	    height: 60px;
	    padding: 5px 0px;
	    background: #d7d2c3;
	    border: 1px solid #c6c6c6;
	    text-align: center;
	    width: 200px;
	    font-size: 11px;
	    color: #53442d;
	    font-weight: bold;
	}
	.quyenloi ul li{
		display: inline-block;
	}
	p{
		color: #636363;
	    font-family: Verdana,Arial,sans-serif;
	    font-size: 14px;
	    line-height: 24px;
	}
	.tabcontent{
		margin-left: 40px;
	}
	.tit{
		color: #53442d;
	    font-size: 20px;
	    font-weight: bold;
	}
</style>