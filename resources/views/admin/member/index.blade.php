@extends('layouts.admin')
@section('contentadmin')
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên</th>
                  <th scope="col">email</th>
                  <th scope="col">Số điện thoại</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
               
                @foreach($members as $member)
                <tr>
                  <th scope="row">{{ $member->id }}</th>
                  <td>{{ $member->fullname }}</td>
                  <td>{{ $member->email }}</td>
                  <td>{{ $member->sdt }}</td>
                  <td>
                  @can('member-delete')
                    <a href="{{ route('members.delete', ['id' => $member->id]) }}"
                    class="btn btn-danger action_delete">Delete</a>
                  @endcan
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
@endsection