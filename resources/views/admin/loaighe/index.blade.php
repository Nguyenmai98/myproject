@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<a href="{{ route('loaighes.create') }}" class="btn btn-success float-right m-2" style="font-size: 14px;">Add</a>
			</div>
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên loại ghế</th>
                  <th scope="col">Đơn giá</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($loaighes as $loaighe)

                <tr>
                  <th scope="row">{{ $loaighe->id }}</th>
                  <td>{{ $loaighe->name }}</td>
                  <td>{{ $loaighe->dongia }}</td>
                  <td>
                    <a href="{{ route('loaighes.edit', ['id' => $loaighe->id]) }}" class="btn btn-default">Edit</a>
                  
                    <a href="{{ route('loaighes.delete', ['id' => $loaighe->id]) }}" class="btn btn-danger">Delete</a>
                 
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	