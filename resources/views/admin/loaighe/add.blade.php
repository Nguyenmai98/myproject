@extends('layouts.admin')
@section('contentadmin')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <form action="{{ route('loaighes.store') }}" method="post"> 
          @csrf
        <div class="form-group">
          <label>Tên loại ghế</label>
          <input type="text" class="form-control" name="name" style="font-size: 14px; height: 40px" placeholder="Nhập tên loại ghế">
        </div>
        <div class="form-group">
          <label>Đơn giá</label>
          <input type="text" class="form-control" name="dongia" style="font-size: 14px; height: 40px" placeholder="Nhập đơn giá">
        </div>
        <div class="col-md-12">
            <div class="form-group">
            <label>Mô tả</label>
            <textarea class="form-control" name="mota" rows="20">
              {{ old('mota') }}
            </textarea>
          </div>
        </div>

        
        <button type="submit" class="btn btn-primary" style="font-size: 14px; height: 40px">Submit</button>
      </form>  
      </div>
                      
    </div>
  </div>
@endsection 