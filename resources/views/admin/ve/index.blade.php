@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên phim</th>
                  <th scope="col">Ngày chiếu</th>
                  <th scope="col">Giờ chiếu</th>
                  <th scope="col">Phòng chiếu</th>
                  <th scope="col">Ghế</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($ves as $ve)

                <tr>
                  <th scope="row">{{ $ve->id }}</th>
                  <td>{{ $ve->phim }}</td>
                  <td>{{ $ve->ngay_chieu }}</td>
                  <td>{{ $ve->gio_chieu }}</td>
                  <td>{{ $ve->phong_chieu }}</td>
                  <td>{{ $ve->ghe }}</td>
                  <td>
                  @can('ve-delete')
                    <a href="{{ route('ves.delete', ['id' => $ve->id]) }}" class="btn btn-danger">Delete</a>
                  @endcan
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	