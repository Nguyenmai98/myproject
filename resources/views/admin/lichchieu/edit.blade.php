@extends('layouts.admin')
@section('contentadmin')
<div class="container-fluid">
  <div class="row"> 
    <div class="col-md-12">
    <form action="{{ route('lichchieus.store') }}" method="post" enctype="multipart/form-data"> 
      @csrf
       <div class="form-group">
              <label>Chọn phim</label>
              <select name="phim" class="form-control">
                
                    <option selected>{{ $lichchieu->phim }}</option>
                    <option value="1">Croods</option>
                    <option value="2">Giải cứu "GUY"</option>
                    <option value="3">Án mạng trên sông Nile</option>
                
              </select>
          </div>
          <div class="form-group">
              <label>Chọn ngày chiếu</label>
              <select name="ngay_chieu" class="form-control">
                
                    <option selected>{{ $lichchieu->ngay_chieu }}</option>
                    <option value="">30/5/2021</option>
                    <option value="">31/5/2021</option>
                    <option value="">1/6/2021</option>
                
              </select>
          </div>
          <div class="form-group">
              <label>Chọn giờ chiếu</label>
              <select name="gio_chieu" class="form-control">
                
                    <option selected>{{ $lichchieu->gio_chieu }}</option>
                    <option value="">18:50</option>
                    <option value="">19:50</option>
                    <option value="">20:50</option>
                
              </select>
          </div>
         
          <div class="form-group">
              <label>Chọn phòng chiếu</label>
              <select name="phong_chieu" class="form-control">
               
                  <option selected>{{ $lichchieu->phong_chieu }}</option>
                  <option>P01</option>
                  <option>P02</option>
                  <option>P03</option>
                
              </select>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection 
