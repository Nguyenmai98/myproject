@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<a href="{{ route('lichchieus.create') }}" class="btn btn-success float-right m-2" style="font-size: 14px;">Add</a>
			</div>
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên phim</th>
                  <th scope="col">Ngày chiếu</th>
                  <th scope="col">Giờ chiếu</th>
                  <th scope="col">Phòng chiếu</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($lichchieus as $lichchieu)

                <tr>
                  <th scope="row">{{ $lichchieu->id }}</th>
                  <td>{{ $lichchieu->phim }}</td>
                  <td>{{ $lichchieu->ngay_chieu }}</td>
                  <td>{{ $lichchieu->gio_chieu }}</td>
                  <td>{{ $lichchieu->phong_chieu }}</td>
                  <td>
                    <a href="{{ route('lichchieus.edit', ['id' => $lichchieu->id]) }}" class="btn btn-default">Edit</a>
                  
                    <a href="{{ route('lichchieus.delete', ['id' => $lichchieu->id]) }}" class="btn btn-danger">Delete</a>
                 
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	