@extends('layouts.admin')
@section('contentadmin')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <form action="{{ route('trangthais.update', ['id' => $trangthai->id]) }}" method="post"> 
          @csrf          
          <div class="form-group">
            <label>Tên trạng thái</label>
            <input type="text" class="form-control" name="name" value="{{ $trangthai->name }}" style="font-size: 14px; height: 40px" placeholder="Nhập tên trạng thái">
          </div>
          <div class="col-md-12">
              <div class="form-group">
              <label>Mô tả</label>
              <textarea class="form-control" name="mota" rows="20">
                {{ $trangthai->mota }}
              </textarea>
            </div>
          </div>
        
        <button type="submit" class="btn btn-primary" style="font-size: 14px; height: 40px">Submit</button>
      </form>  
      </div>
                      
    </div>
  </div>
@endsection