@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<a href="{{ route('trangthais.create') }}" class="btn btn-success float-right m-2" style="font-size: 14px;">Add</a>
			</div>
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên trạng thái</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($trangthais as $trangthai)

                <tr>
                  <th scope="row">{{ $trangthai->id }}</th>
                  <td>{{ $trangthai->name }}</td>
                  <td>
                    <a href="{{ route('trangthais.edit', ['id' => $trangthai->id]) }}" class="btn btn-default">Edit</a>
                  
                    <a href="{{ route('trangthais.delete', ['id' => $trangthai->id]) }}" class="btn btn-danger">Delete</a>
                 
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	