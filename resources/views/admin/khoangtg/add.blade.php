@extends('layouts.admin')
@section('contentadmin')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <form action="{{ route('khoangtgs.store') }}" method="post"> 
          @csrf
        <div class="form-group">
          <label>Ngày chiếu</label>
          <input type="date" class="form-control" name="ngaychieu" style="font-size: 14px; height: 40px" placeholder="Nhập ngày chiếu">
          <div class="form-group">
            <label>Giờ chiếu</label>
            <input type="time" class="form-control" name="giochieu" style="font-size: 14px; height: 40px" placeholder="Nhập giờ chiếu">
          </div>
        </div>
        
        <button type="submit" class="btn btn-primary" style="font-size: 14px; height: 40px">Submit</button>
      </form>  
      </div>
                      
    </div>
  </div>
@endsection 