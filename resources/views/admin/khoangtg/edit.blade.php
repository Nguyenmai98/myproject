@extends('layouts.admin')
@section('contentadmin')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <form action="{{ route('khoangtgs.update', ['id' => $khoangtg->id]) }}" method="post"> 
          @csrf
        <div class="form-group">
          <label>Ngày chiếu</label>
          <input type="date" class="form-control" name="ngaychieu" value="{{ $khoangtg->ngaychieu }}" style="font-size: 14px; height: 40px" placeholder="Nhập ngày chiếu">
          <div class="form-group">
            <label>Giờ chiếu</label>
            <input type="text" class="form-control" name="giochieu" value="{{ $khoangtg->giochieu }}" style="font-size: 14px; height: 40px" placeholder="Nhập giờ chiếu">
          </div>
          </div>
        
        <button type="submit" class="btn btn-primary" style="font-size: 14px; height: 40px">Submit</button>
      </form>  
      </div>
                      
    </div>
  </div>
@endsection