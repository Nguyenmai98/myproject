@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<a href="{{ route('khoangtgs.create') }}" class="btn btn-success float-right m-2" style="font-size: 14px;">Add</a>
			</div>
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Ngày chiếu</th>
                  <th scope="col">Giờ chiếu</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($khoangtgs as $khoangtg)

                <tr>
                  <th scope="row">{{ $khoangtg->id }}</th>
                  <td>{{ $khoangtg->ngaychieu }}</td>
                  <td>{{ $khoangtg->giochieu }}</td>
                  <td>
                    <a href="{{ route('khoangtgs.edit', ['id' => $khoangtg->id]) }}" class="btn btn-default">Edit</a>
                  
                    <a href="{{ route('khoangtgs.delete', ['id' => $khoangtg->id]) }}" class="btn btn-danger">Delete</a>
                 
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	