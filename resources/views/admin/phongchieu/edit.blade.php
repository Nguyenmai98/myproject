@extends('layouts.admin')
@section('contentadmin')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <form action="{{ route('phongchieus.update', ['id' => $phongchieu->id]) }}" method="post"> 
          @csrf
        <div class="form-group">
          <label>Tên phòng</label>
          <input type="text" class="form-control" name="name" value="{{ $phongchieu->name }}" style="font-size: 14px; height: 40px" placeholder="Nhập tên phòng">
          <div class="form-group">
            <label>Tổng số ghế</label>
            <input type="text" class="form-control" name="tongsoghe" value="{{ $phongchieu->tongsoghe }}" style="font-size: 14px; height: 40px" placeholder="Nhập số ghế">
          </div>
          </div>
        
        <button type="submit" class="btn btn-primary" style="font-size: 14px; height: 40px">Submit</button>
      </form>  
      </div>
                      
    </div>
  </div>
@endsection