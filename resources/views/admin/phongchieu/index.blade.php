@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<a href="{{ route('phongchieus.create') }}" class="btn btn-success float-right m-2" style="font-size: 14px;">Add</a>
			</div>
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên phòng</th>
                  <th scope="col">Tổng số ghế</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($phongchieus as $phongchieu)

                <tr>
                  <th scope="row">{{ $phongchieu->id }}</th>
                  <td>{{ $phongchieu->name }}</td>
                  <td>{{ $phongchieu->tongsoghe }}</td>
                  <td>
                    <a href="{{ route('phongchieus.edit', ['id' => $phongchieu->id]) }}" class="btn btn-default">Edit</a>
                  
                    <a href="{{ route('phongchieus.delete', ['id' => $phongchieu->id]) }}" class="btn btn-danger">Delete</a>
                 
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	