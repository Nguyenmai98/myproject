@extends('layouts.admin')
@section('contentadmin')
<div class="container-fluid">
  <div class="row"> 
    <div class="col-md-12">
    <form action="{{ route('servers.update', ['id' => $server->id]) }}" method="post" enctype="multipart/form-data"> 
      @csrf
            <div class="form-group">
              <label>Tên dịch vụ</label>
              <input type="text" 
              class="form-control" name="name" placeholder="Nhập tên dịch vụ"
              value="{{ $server->name }}">
            </div>
            <div class="form-group">
              <label>Ảnh dịch vụ</label>
              <input type="file" class="form-control-file" name="image_path">
              <div class="col-md-12">
                <div class="row">
                  <img class="product_img_150_100" src="{{ $server->image_path }}">
                </div>
              </div>
            </div>
            <div class="col-md-12">
                  <div class="form-group">
                  <label>Mô tả</label>
                  <textarea class="form-control" name="descrip" rows="20">
                    {{ $server->descrip }}
                  </textarea>
                </div>
              </div>
            <div class="form-group">
              <label>Ngày hiệu lực</label>
              <input type="text" 
              class="form-control" name="ngayhieuluc" placeholder="Nhập ngày hiệu lực"
              value="{{ $server->ngayhieuluc }}">
            </div>
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
        </form>
      </div>
    </div>
  </div>
@endsection 
