@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<a href="{{ route('servers.create') }}" class="btn btn-success float-right m-2" style="font-size: 14px;">Add</a>
			</div>
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên dịch vụ</th>
                  <th scope="col">Hình ảnh</th>
                  <th scope="col">Mô tả</th>
                  <th scope="col">Ngày hiệu lực</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($servers as $serverItem)

                <tr>
                  <th scope="row">{{ $serverItem->id }}</th>
                  <td>{{ $serverItem->name }}</td>
                  <td>
                    <img class="product_image_150_100" src="{{ $serverItem->image_path }}">
                  </td>
                  <td>{{ $serverItem->descrip }}</td>
                  <td>{{ $serverItem->ngayhieuluc }}</td>
                  <td>
                    <a href="{{ route('servers.edit', ['id' => $serverItem->id]) }}" class="btn btn-default">Edit</a>
                    <a href="" 
                    data-url="{{ route('servers.delete', ['id' => $serverItem->id]) }}"
                    class="btn btn-danger action_delete">Delete</a>
                  </td>
                </tr>
                @endforeach 
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	
@section('js')
<script src="{{ asset('admins/list.js') }}"></script>
@endsection
<style type="text/css">
  .product_image_150_100{
    width: 150px;
  }
</style>