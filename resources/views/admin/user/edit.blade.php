@extends('layouts.admin')
@section('contentadmin')
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <form action="{{ route('users.update', ['id' => $user->id]) }}" method="post" enctype="multipart/form-data"> 
              @csrf
            <div class="form-group">
              <label>Tên</label>
              <input type="text" 
              class="form-control" 
              name="name" 
              placeholder="Nhập tên"
              value="{{ $user->name }}">
            </div>

            <div class="form-group">
              <label>Email</label>
              <input type="email" 
              class="form-control" 
              name="email" 
              placeholder="Nhập email"
              value="{{ $user->email }}">
            </div>

            <div class="form-group">
              <label>Password</label>
              <input type="password" 
              class="form-control" 
              name="password" 
              placeholder="Nhập mật khẩu">
            </div>

            <div class="form-group">
              <label>Chọn vai trò</label>
              <select name="role_id[]" class="form-control select2" multiple="multiple">
                <option value="">
                  @foreach($roles as $role)
                    <option 
                    {{ $roleOfUser->contains('id', $role->id) ? 'selected' : '' }}
                    value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </option>
              </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </form>  
          </div>
                          
        </div>
      </div>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
  });
  </script>
@endsection