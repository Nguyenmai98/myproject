@extends('layouts.admin')
@section('contentadmin')
<div class="container-fluid">
  <div class="row"> 
    <div class="col-md-12">
    <form action="{{ route('roles.update', ['id' => $role->id]) }}" method="post" enctype="multipart/form-data"> 
      @csrf
            <div class="form-group">
              <label>Tên vai trò</label>
              <input type="text" 
              class="form-control" name="name" placeholder="Nhập tên vai trò"
              value="{{ $role->name }}">
            </div>
            <div class="form-group">
            <label>Mô tả vai trò</label>
            <textarea class="form-control" name="display_name" rows="4">
              {{ $role->display_name }}
            </textarea>
            </div>
            @foreach($permissionParent as $permissionParentItem)
              <div class="card border-primary mb-3 col-md-12">
                <div class="card-header">
                  <label><input type="checkbox" class="checkbox_wrapper" name=""></label>
                  Module {{ $permissionParentItem->name }}
                </div>

                <div class="row">
                @foreach($permissionParentItem->permissionChildren as $permissionChildrenItem)
                  <div class="card-body text-primary col-md-3">
                    <div class="card-title">
                      <label><input type="checkbox" class="checkbox_children" 
                        name="permission_id[]" 
                        {{ $permissionChecked->contains('id', $permissionChildrenItem->id) ? 'checked' : '' }}
                       value="{{ $permissionChildrenItem->id }}"></label>
                      {{ $permissionChildrenItem->name }}
                    </div>
                  </div>
                @endforeach
                </div>
                </div>
            @endforeach
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript">
  $('.checkbox_wrapper').on('click', function(){
    $(this).parents('.card').find('.checkbox_children').prop('checked', $(this).prop('checked'));
  });
</script>
@endsection 
