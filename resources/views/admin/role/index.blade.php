@extends('layouts.admin')
@section('contentadmin')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
        @can('role-add')
				<a href="{{ route('roles.create') }}" class="btn btn-success float-right m-2" style="font-size: 14px;">Add</a>
        @endcan
			</div>
			<div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tên vai trò</th>
                  <th scope="col">Mô tả</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>

                @foreach($roles as $role)

                <tr>
                  <th scope="row">{{ $role->id }}</th>
                  <td>{{ $role->name }}</td>
                  <td>{{ $role->display_name }}</td>
                  <td>
                  @can('role-edit')
                    <a href="{{ route('roles.edit', ['id' => $role->id]) }}" class="btn btn-default">Edit</a>
                  @endcan
                  @can('role-delete')
                    <a href="" 
                    data-url="{{ route('roles.delete', ['id' => $role->id]) }}"
                    class="btn btn-danger">Delete</a>
                  @endcan
                  </td>
                </tr>
                @endforeach 
              </tbody>
            </table>
          </div>
		</div>
	</div>
@endsection	