<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CreateRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
        	['name' => 'admin', 'display_name' => 'Quản trị hệ thống'],
        	['name' => 'nhanvien', 'display_name' => 'Nhân viên rạp'],
        	['name' => 'quanlyrap', 'display_name' => 'Quản lý rạp'],
        ]);
    }
}
