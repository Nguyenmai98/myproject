<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ve;

class VeController extends Controller
{
    private $ve;
    public function __construct(Ve $ve){
        $this->ve = $ve;
    }
    public function index(){
    	$ves = $this->ve->paginate(10);
    	return view('admin.ve.index', compact('ves'));
    }
    
    public function delete($id){
    	$this->ve->find($id)->delete();
    	return redirect()-> route('ves.index');
    }
}
