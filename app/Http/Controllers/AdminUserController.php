<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use DB;
use App\Components\Recusive;
use Illuminate\Support\Str;

class AdminUserController extends Controller
{
	private $user;
    private $role;
	public function __construct(User $user, Role $role){
		$this->user = $user;
        $this->role = $role;
	}
    public function index(){
    	$users = $this->user->paginate(20);
    	return view('admin.user.index', compact('users'));
    }

    public function create(){
        $roles = $this->role->all();
    	return view('admin.user.add', compact('roles'));
    }

    public function store(Request $request){
    	$user = $this->user->create([
    		'name' => $request->name,
    		'email' => $request->email,
    		'password' => Hash::make($request->password)
    		]);
        $roleIds = $request->role_id;
        foreach ($roleIds as $roleItem) {
           DB::table('role_user')->insert([
            'role_id' => $roleItem,
            'user_id' => $user->id
        ]);
        }
    	return redirect()->route('users.index');
    
    }

    public function edit(Request $request, $id){
    	$user = $this->user->find($id);
        $roles = $this->role->all();
        $roleOfUser = $user->roles;
    	return view('admin.user.edit', compact('user', 'roles', 'roleOfUser'));
    }

    public function update(Request $request, $id){
    	$this->user->find($id)->update([
    		'name' => $request->name,
    		'email' => $request->email,
    		'password' => Hash::make($request->password)
    		]);
        $user = $this->user->find($id);
        $user->roles()->sync($request->role_id);
    	return redirect()->route('users.index');
    	
    }

    public function delete($id){
    	$this->user->find($id)->delete();
    	return redirect()-> route('users.index');
    }
}
