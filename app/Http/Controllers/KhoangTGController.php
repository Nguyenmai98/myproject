<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KhoangTG;
use Illuminate\Support\Str;

class KhoangTGController extends Controller
{
    private $khoangtg;

	public function __construct(KhoangTG $khoangtg){
		$this->khoangtg = $khoangtg;
	}

    public function create()
    {
    	return view('admin.khoangtg.add');
    }
    
    public function index(){
    	$khoangtgs = $this->khoangtg->latest()->paginate(25);
    	return view('admin.khoangtg.index', compact('khoangtgs'));
    }

    public function store(Request $request){
    	$this->khoangtg->create([
    		'ngaychieu' => $request->ngaychieu,
    		'giochieu' => $request->giochieu,
    	]);

    	return redirect()-> route('khoangtgs.index');
    }

    public function edit($id){
    	$khoangtg = $this->khoangtg->find($id);
    	return view('admin.khoangtg.edit', compact('khoangtg'));
    }

    public function update($id, Request $request){
    	$this->khoangtg->find($id)->update([
    		'ngaychieu' => $request->ngaychieu,
    		'giochieu' => $request->giochieu,
    	]);
    	return redirect()-> route('khoangtgs.index');
    }

    public function delete($id){
    	$this->khoangtg->find($id)->delete();
    	return redirect()-> route('khoangtgs.index');
    }
}
