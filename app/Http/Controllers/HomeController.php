<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Product;
use App\Models\Category;
use App\Models\Server;

class HomeController extends Controller
{
    public function index(){
    	$sliders = Slider::get();
    	$products = Product::get();
        $servers = Server::latest()->take(4)->get();
    	return view('home', compact('sliders', 'products', 'servers'));
    }

    public function productsDetail(Request $req){
    	$products = Product::where('id', $req->id)->first();
    	return view('chitietphim', compact('products'));
    }


    public function loaiPhim($type){
    	$categories = Product::where('category_id', $type)->get();
    	return view('loaiphim', compact('categories'));
    }

    public function tinTuc(){
       $servers = Server::latest()->get();
        return view('news', compact('servers'));
    }

    public function chiTietTin(Request $req){
       $servers = Server::where('id', $req->id)->first();
        return view('chitietdichvu', compact('servers'));
    }
}
