<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LichChieuPhim;

class LichChieuPhimController extends Controller
{
    private $lichchieu;
    public function __construct(LichChieuPhim $lichchieu){
        $this->lichchieu = $lichchieu;
    }
    public function index(){
    	$lichchieus = $this->lichchieu->paginate(10);
    	return view('admin.lichchieu.index', compact('lichchieus'));
    }
    public function create(){
		return view('admin.lichchieu.add');
	}
    public function store(Request $request){
    	
    	return redirect()-> route('lichchieus.index');
    }

    public function edit($id){
    	$lichchieu = $this->lichchieu->find($id);
    	return view('admin.lichchieu.edit', compact('lichchieu'));
    }

    public function update(Request $request, $id){
    }

    public function delete($id){
    	$this->lichchieu->find($id)->delete();
    	return redirect()-> route('lichchieus.index');
    }
}
