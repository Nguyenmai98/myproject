<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Components\Recusive;
use App\Models\Server;
use Illuminate\Support\Facades\Storage;
use App\Traits\StorageImageTrait;
use DB;
use Log;
use App\Traits\DeleteModelTrait;

class ServerController extends Controller
{
    use DeleteModelTrait;
	use StorageImageTrait;
	public function __construct(Server $server){
		$this->server = $server;
	}
	public function index(){
		$servers = $this->server->paginate(25);
		return view('admin.server.index', compact('servers'));
	}

	public function create(){
		return view('admin.server.add');
	}

    public function store(Request $request){
    		$dataServerCreate = [
	    		'name' => $request->name,
	    		'descrip' => $request->descrip,
	    		'ngayhieuluc' => $request->ngayhieuluc,
		    	];
		    	$dataUploadImage = $this->storageTraitUpload($request, 'image_path', 'server');
		    	if(!empty($dataUploadImage)){
		    		$dataServerCreate['image_name'] = $dataUploadImage['file_name'];
		    		$dataServerCreate['image_path'] = $dataUploadImage['file_path'];
		    	}
		    	$server = $this->server->create($dataServerCreate);
		    	return redirect()->route('servers.index');

    	}

    public function edit($id){
    	$server = $this->server->find($id);
    	return view('admin.server.edit', compact('server'));
    }

    public function update(Request $request, $id){
    	
    		$dataServerUpdate = [
	    		'name' => $request->name,
	    		'descrip' => $request->descrip,
	    		'ngayhieuluc' => $request->ngayhieuluc,
		    	];
		    	$dataUploadImage = $this->storageTraitUpload($request, 'image_path', 'server');
		    	if(!empty($dataUploadImage)){
		    		$dataServerUpdate['image_name'] = $dataUploadImage['file_name'];
		    		$dataServerUpdate['image_path'] = $dataUploadImage['file_path'];
		    	}
		    	$this->server->find($id)->update($dataServerUpdate);
		    	$server = $this->server->find($id);
		    	DB::commit();
		    	return redirect()->route('servers.index');
    }
    public function delete($id){
    	return $this->deleteModelTrait($id, $this->server);
    }
}
