<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trangthaighe;
use Illuminate\Support\Str;

class TrangThaiController extends Controller
{
    private $trangthai;

	public function __construct(Trangthaighe $trangthai){
		$this->trangthai = $trangthai;
	}

    public function create()
    {
    	return view('admin.trangthai.add');
    }
    
    public function index(){
    	$trangthais = $this->trangthai->latest()->paginate(25);
    	return view('admin.trangthai.index', compact('trangthais'));
    }

    public function store(Request $request){
    	$this->trangthai->create([
    		'name' => $request->name,
    		'mota' => $request->mota,
    	]);

    	return redirect()-> route('trangthais.index');
    }

    public function edit($id){
    	$trangthai = $this->trangthai->find($id);
    	return view('admin.trangthai.edit', compact('trangthai'));
    }

    public function update($id, Request $request){
    	$this->trangthai->find($id)->update([
    		'name' => $request->name,
    		'mota' => $request->mota,
    	]);
    	return redirect()-> route('trangthais.index');
    }

    public function delete($id){
    	$this->trangthai->find($id)->delete();
    	return redirect()-> route('trangthais.index');
    }
}
