<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use Illuminate\Support\Str;

class MemberController extends Controller
{
    private $member;

	public function __construct(Member $member){
		$this->member = $member;
	}

    public function create()
    {
    	return view('login');
    }
    
    public function index(){
    	$members = $this->member->latest()->paginate(25);
    	return view('admin.member.index', compact('members'));
    }

    public function store(Request $request){
    	$this->member->create([
    		'fullname' => $request->fullname,
            'password' => $request->password,
            'email' => $request->email,
            'sdt' => $request->sdt,
            'gioitinh' => $request->gioitinh,
            'ngaysinh' => $request->ngaysinh,
    	]);

    	return redirect()-> route('members.index');
    }

    public function edit($id){
    	$member = $this->member->find($id);
    	return view('', compact('member'));
    }

    public function update($id, Request $request){
    	$this->member->find($id)->update([
    		'fullname' => $request->fullname,
            'password' => $request->password,
            'email' => $request->email,
            'sdt' => $request->sdt,
            'gioitinh' => $request->gioitinh,
            'ngaysinh' => $request->ngaysinh,
    	]);
    	return redirect()-> route('members.index');
    }

    public function delete($id){
    	$this->member->find($id)->delete();
    	return redirect()-> route('members.index');
    }
}
