<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phongchieu;
use App\Components\Recusive;
use Illuminate\Support\Str;

class PhongChieuController extends Controller
{
    private $phongchieu;

	public function __construct(Phongchieu $phongchieu){
		$this->phongchieu = $phongchieu;
	}

    public function create()
    {
    	return view('admin.phongchieu.add');
    }
    
    public function index(){
    	$phongchieus = $this->phongchieu->latest()->paginate(25);
    	return view('admin.phongchieu.index', compact('phongchieus'));
    }

    public function store(Request $request){
    	$this->phongchieu->create([
    		'name' => $request->name,
    		'tongsoghe' => $request->tongsoghe,
    	]);

    	return redirect()-> route('phongchieus.index');
    }

    public function edit($id){
    	$phongchieu = $this->phongchieu->find($id);
    	return view('admin.phongchieu.edit', compact('phongchieu'));
    }

    public function update($id, Request $request){
    	$this->phongchieu->find($id)->update([
    		'name' => $request->name,
    		'tongsoghe' => $request->tongsoghe,
    	]);
    	return redirect()-> route('phongchieus.index');
    }

    public function delete($id){
    	$this->phongchieu->find($id)->delete();
    	return redirect()-> route('phongchieus.index');
    }
}
