<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loaighe;
use Illuminate\Support\Str;

class LoaiGheController extends Controller
{
    private $loaighe;

	public function __construct(Loaighe $loaighe){
		$this->loaighe = $loaighe;
	}

    public function create()
    {
    	return view('admin.loaighe.add');
    }
    
    public function index(){
    	$loaighes = $this->loaighe->latest()->paginate(25);
    	return view('admin.loaighe.index', compact('loaighes'));
    }

    public function store(Request $request){
    	$this->loaighe->create([
    		'name' => $request->name,
    		'mota' => $request->mota,
    		'dongia' => $request->dongia,
    	]);

    	return redirect()-> route('loaighes.index');
    }

    public function edit($id){
    	$loaighe = $this->loaighe->find($id);
    	return view('admin.loaighe.edit', compact('loaighe'));
    }

    public function update($id, Request $request){
    	$this->loaighe->find($id)->update([
    		'name' => $request->name,
    		'mota' => $request->mota,
    		'dongia' => $request->dongia,
    	]);
    	return redirect()-> route('loaighes.index');
    }

    public function delete($id){
    	$this->loaighe->find($id)->delete();
    	return redirect()-> route('loaighes.index');
    }
}
