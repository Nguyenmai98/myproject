<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('user-list', function ($user) {
            return $user->checkPermissionAccess('list_user');
        });
        Gate::define('user-add', function ($user) {
            return $user->checkPermissionAccess('add_user');
        });
        Gate::define('user-edit', function ($user) {
            return $user->checkPermissionAccess('edit_user');
        });
        Gate::define('user-delete', function ($user) {
            return $user->checkPermissionAccess('delete_user');
        });
        Gate::define('member-delete', function ($user) {
            return $user->checkPermissionAccess('delete_member');
        });
        Gate::define('role-list', function ($user) {
            return $user->checkPermissionAccess('list_role');
        });
        Gate::define('role-add', function ($user) {
            return $user->checkPermissionAccess('add_role');
        });
        Gate::define('role-edit', function ($user) {
            return $user->checkPermissionAccess('edit_role');
        });
        Gate::define('role-delete', function ($user) {
            return $user->checkPermissionAccess('delete_role');
        });
        Gate::define('ve-delete', function ($user) {
            return $user->checkPermissionAccess('delete_ve');
        });
    }
}
